package safin.kamil.ocr_reader.settings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import safin.kamil.ocr_reader.base.BaseItemHolder;
import safin.kamil.ocr_reader.base.BaseListAdapter;
import safin.kamil.ocr_reader.database.User;

public class UserAdapter extends BaseListAdapter<User> {

    private Context context;

    private OnUserClickListener<User> onUserClickListener;

    public UserAdapter(Context context, OnUserClickListener<User> onUserClickListener) {
        super(new BaseItemHolder.OnItemClickListener<User>() {
            @Override
            public void onItemClick(User item) {

            }
        });
        this.onUserClickListener = onUserClickListener;
        this.context = context;
    }

    public List<User> getUsers() {
        return items;
    }

    @Override
    public BaseItemHolder<User> createBaseItemHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(safin.kamil.ocr_reader.R.layout.row_user, parent, false);
        return new UserHolder(view);
    }

    class UserHolder extends BaseItemHolder<User> {

        @BindView(safin.kamil.ocr_reader.R.id.name)
        TextView name;

        @BindView(safin.kamil.ocr_reader.R.id.surname)
        TextView surname;

        public UserHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onUserClickListener != null) {
                        onUserClickListener.onItemClick(item);
                    }
                }
            });

            itemView.findViewById(safin.kamil.ocr_reader.R.id.remove).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onUserClickListener != null) {
                        onUserClickListener.onRemoveClick(item);
                    }
                }
            });
        }

        @Override
        protected void onItemBind(User item) {
            name.setText(item.getName());
            surname.setText(item.getSurname());
        }
    }

    public interface OnUserClickListener<T> {
        void onItemClick(T item);

        void onRemoveClick(T item);
    }
}
