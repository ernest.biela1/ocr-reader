package safin.kamil.ocr_reader.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import safin.kamil.ocr_reader.R;
import safin.kamil.ocr_reader.SharedPref;
import safin.kamil.ocr_reader.database.User;
import safin.kamil.ocr_reader.database.User_Table;
import safin.kamil.ocr_reader.network.CachedCarsProvider;
import safin.kamil.ocr_reader.network.LoadCarsProvider;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.name)
    EditText nameText;

    @BindView(R.id.surname)
    EditText surnameText;

    @BindView(R.id.empty_name)
    View emptyName;

    @BindView(R.id.empty_surname)
    View emptySurname;

    @BindView(R.id.user_list)
    RecyclerView userList;

    private UserAdapter userAdapter;

    public static void start(Context context) {
        Intent intent = new Intent(context, SettingsActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);

        final String userName = SharedPref.getUserName(this);
        final String userSurname = SharedPref.getUserSurname(this);

        if (userName != null && userSurname != null) {
            getSupportActionBar().setTitle(userName + " " + userSurname);
        } else {
            getSupportActionBar().setTitle(getString(R.string.no_user));
        }

        nameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null && !charSequence.toString().isEmpty()) {
                    emptyName.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        surnameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null && !charSequence.toString().isEmpty()) {
                    emptySurname.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        userList.setLayoutManager(new LinearLayoutManager(this));

        userAdapter = new UserAdapter(this, new UserAdapter.OnUserClickListener<User>() {
            @Override
            public void onItemClick(User item) {
                SharedPref.setUserName(SettingsActivity.this, item.getName());
                SharedPref.setUserSurname(SettingsActivity.this, item.getSurname());
                getSupportActionBar().setTitle(item.getName() + " " + item.getSurname());
                onBackPressed();
            }

            @Override
            public void onRemoveClick(User item) {
                FlowManager.getModelAdapter(User.class).delete(item);

                if (item.getName().toLowerCase().trim().equals(userName.toLowerCase().trim())
                        && item.getSurname().toLowerCase().trim().equals(userSurname.toLowerCase().trim())) {

                    SharedPref.setUserName(SettingsActivity.this, null);
                    SharedPref.setUserSurname(SettingsActivity.this, null);
                    getSupportActionBar().setTitle(getString(R.string.no_user));
                }

                loadList();
            }
        });

        userList.setAdapter(userAdapter);
        loadList();
    }

    private void loadList() {
        SQLite.select()
                .from(User.class)
                .orderBy(User_Table.surname, true)
                .async()
                .queryListResultCallback(new QueryTransaction.QueryResultListCallback<User>() {
                    @Override
                    public void onListQueryResult(QueryTransaction transaction, @NonNull List<User> tResult) {
                        userAdapter.setData(tResult);
                    }
                }).execute();
    }

    @OnClick(R.id.add)
    void onAddClick() {
        String userName = nameText.getText().toString();
        String userSurname = surnameText.getText().toString();

        boolean valid = true;

        if (userName.isEmpty()) {
            emptyName.setVisibility(View.VISIBLE);
            valid = false;
        }

        if (userSurname.isEmpty()) {
            emptySurname.setVisibility(View.VISIBLE);
            valid = false;
        }

        if (valid) {

            User repeat = null;

            for (User item : userAdapter.getUsers()) {
                if (item.getName().toLowerCase().trim().equals(userName.toLowerCase().trim())
                        && item.getSurname().toLowerCase().trim().equals(userSurname.toLowerCase().trim())) {
                    repeat = item;
                }
            }

            User user = new User(userName, userSurname);
            if (repeat != null) {
                repeat.setName(userName);
                repeat.setSurname(userSurname);
                FlowManager.getModelAdapter(User.class).update(repeat);
            } else {
                FlowManager.getModelAdapter(User.class).save(user);
            }

            nameText.setText(null);
            surnameText.setText(null);

            loadList();
        }
    }

    @OnClick(R.id.update_cars)
    void onUpdateCarsClick() {
        SharedPref.setLastUpdate(this, -1);
        new CachedCarsProvider(this).loadCars(new LoadCarsProvider.Callback() {
            @Override
            public void onSuccess(List<String> cars, boolean cached) {
                Snackbar.make(findViewById(android.R.id.content), R.string.cars_updated, Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(findViewById(android.R.id.content), R.string.loading_cars_error, Snackbar.LENGTH_LONG).show();
            }
        });
    }
}
