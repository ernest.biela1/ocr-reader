package safin.kamil.ocr_reader.model;

public class Device {

    private String plate;
    private String phone;
    private String description;

    public Device(String plate, String phone, String description) {
        this.plate = plate;
        this.phone = phone;
        this.description = description;
    }

    public String getPlate() {
        return plate;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }
}
