package safin.kamil.ocr_reader.model;

public class Form {
    private String name;
    private String surname;
    private String meter_status;
    private String start_stop;
    private String location;
    private String car;

    public Form(String name, String surname, String meter_status, String start_stop, String location, String car) {
        this.name = name;
        this.surname = surname;
        this.meter_status = meter_status;
        this.start_stop = start_stop;
        this.location = location;
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMeter_status() {
        return meter_status;
    }

    public String getStart_stop() {
        return start_stop;
    }

    public String getLocation() {
        return location;
    }

    public String getCar() {
        return car;
    }
}
