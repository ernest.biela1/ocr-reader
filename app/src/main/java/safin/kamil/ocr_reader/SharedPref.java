package safin.kamil.ocr_reader;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {

    private static final String SHARED_PREFERENCES = "ocr_shared_preferences";
    private static final String USER_NAME = "USER_NAME";
    private static final String USER_SURNAME = "USER_SURNAME";
    private static final String LAST_UPDATE = "LAST_UPDATE";
    private static final String LAST_SELECTED = "LAST_SELECTED";

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static void setUserName(Context context, String userName) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(USER_NAME, userName);
        sharedPreferencesEditor.apply();
    }

    public static String getUserName(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.getString(USER_NAME, null);
    }

    public static void setUserSurname(Context context, String userSurname) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(USER_SURNAME, userSurname);
        sharedPreferencesEditor.apply();
    }

    public static String getUserSurname(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.getString(USER_SURNAME, null);
    }

    public static void setLastUpdate(Context context, long time) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putLong(LAST_UPDATE, time);
        sharedPreferencesEditor.apply();
    }

    public static long getLastUpdateTime(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.getLong(LAST_UPDATE, -1);
    }

    public static void setLastSelected(Context context, int position) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putInt(LAST_SELECTED, position);
        sharedPreferencesEditor.apply();
    }

    public static int getLastSelected(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.getInt(LAST_SELECTED, 0);
    }
}
