package safin.kamil.ocr_reader.network;

import android.content.Context;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import safin.kamil.ocr_reader.SharedPref;
import safin.kamil.ocr_reader.database.Car;

public class CachedCarsProvider implements LoadCarsProvider {

    private static final long DAY = 1000 * 60 * 60 * 24;

    private Context context;
    private NetworkCarsProvider networkCarsProvider;

    public CachedCarsProvider(Context context) {
        this.context = context;
        networkCarsProvider = new NetworkCarsProvider();
    }

    @Override
    public void loadCars(final Callback callback) {
        if (shouldUpdate()) {
            networkCarsProvider.loadCars(new Callback() {
                @Override
                public void onSuccess(List<String> devices, boolean cached) {
                    Delete.table(Car.class);
                    for (String device : devices) {
                        Car car = new Car(device);
                        FlowManager.getModelAdapter(Car.class).insert(car);
                    }
                    SharedPref.setLastUpdate(context, System.currentTimeMillis());
                    callback.onSuccess(devices, false);
                }

                @Override
                public void onFailure(Throwable t) {
                    callback.onFailure(t);
                }
            });
        } else {
            List<Car> cars = SQLite.select().from(Car.class).queryList();
            List<String> carNames = new ArrayList<>();
            for (Car car : cars) {
                carNames.add(car.getName());
            }
            callback.onSuccess(carNames, true);
        }
    }

    private boolean shouldUpdate() {
        long currentTime = System.currentTimeMillis();
        long lastTime = SharedPref.getLastUpdateTime(context);

        return (currentTime - lastTime) > DAY;
    }
}
