package safin.kamil.ocr_reader.network;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import safin.kamil.ocr_reader.model.Device;

public interface CarsResponse {

    @GET("v1/get_devices")
    Call<List<Device>> getCarsResponse();
}
