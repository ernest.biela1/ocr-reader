package safin.kamil.ocr_reader.network;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import safin.kamil.ocr_reader.model.Device;

class NetworkCarsProvider implements LoadCarsProvider {

    private Retrofit carsLoader;

    NetworkCarsProvider() {
        carsLoader = new Retrofit.Builder()
                .baseUrl("https://www.szafka.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Override
    public void loadCars(final Callback callback) {
        CarsResponse carsResponse = carsLoader.create(CarsResponse.class);
        carsResponse.getCarsResponse().enqueue(new retrofit2.Callback<List<Device>>() {
            @Override
            public void onResponse(Call<List<Device>> call, Response<List<Device>> response) {
                List<String> cars = new ArrayList<>();
                if (response != null && response.body() != null) {
                    for (Device device : response.body()) {
                        cars.add(device.getDescription());
                    }
                    callback.onSuccess(cars, false);
                } else {
                    callback.onFailure(new NullPointerException());
                }
            }

            @Override
            public void onFailure(Call<List<Device>> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }
}
