package safin.kamil.ocr_reader.network;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import safin.kamil.ocr_reader.model.Form;

public class FormsProvider {

    private Retrofit retrofit;

    public FormsProvider() {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://docs.google.com/forms/d/e/")
                .build();
    }

    public void sendForm(Form form, final Callback callback) {
        DocsService spreadsheetWebService = retrofit.create(DocsService.class);
        Call<Void> completeQuestionnaireCall =
                spreadsheetWebService.sendToDocs(form.getName(), form.getSurname(), form.getMeter_status(),
                        form.getStart_stop(), form.getLocation(), form.getCar());

        completeQuestionnaireCall.enqueue(new retrofit2.Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                callback.onSuccess();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                callback.onFailure();
            }
        });
    }

    public interface Callback {
        void onSuccess();

        void onFailure();
    }
}
